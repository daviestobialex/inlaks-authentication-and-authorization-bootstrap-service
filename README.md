# Authentication and Authorization Bootstrap Service

This project is designed to be a starting point on any authentication and authorization service to be deployed in the future


>`version 0.0.1`

# Stack

![](https://img.shields.io/badge/mysql-✓-blue.svg) 
![](https://img.shields.io/badge/java_8-✓-blue.svg)
![](https://img.shields.io/badge/spring_boot-✓-blue.svg)
![](https://img.shields.io/badge/swagger_2-✓-blue.svg)


The following Technology stack was used in this project :

**Java 8** : Core language used

**SpringBoot 2.0** : Displays the REST API

**MySql 5.5** : Database access

## How to use

The project can be configured to run in two modes:
**1** default mode
**2** controlled mode

To which between modes Open `rolesConfig.properties` in config folder at root directory, and in the file change `authServiceType` to either *'controlled'* or *'default'*

if service is in default mode do make sure to manually add the default roles and privileges as per requirment inside the  `rolesConfig.properties` `authRoles` and `authPrivileges` respectively.

# Example
```java
#This service can either start as  a one of the two service types. Either it is default or controlled
authServiceType=default

# If authServiceType is default then set custom roles below. seperate list of roles with comma(*)
# If  Controlled default role for controlled is admin and creates admin user and admin password with no privileges
# for exmaple : 
# authRoles=admin*agents
authRoles=admin;agents

# If authServiceType is default then set custom privileges below. seperate list of privileges with comma(,) and list of roles by roles with (*)
# for exmaple : 
# authPrivileges=create,delete,edit,read,write*edit,read
authPrivileges=read,write;read
```
# Steps To Run

**1** - Do a git clone of the project : 
**2** - Run project from IDE