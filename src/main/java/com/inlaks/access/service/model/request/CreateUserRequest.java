/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.model.request;

import lombok.Data;
import org.inlaks.dto.BaseRequest;

/**
 *
 * @author ailori
 */
@Data
public class CreateUserRequest extends BaseRequest {

    private String firstName;

    private String lastName;

    private long phoneNumber;

    private String email;

    private String password;
}
