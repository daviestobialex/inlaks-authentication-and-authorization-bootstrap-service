/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.model;

import static com.inlaks.access.service.constants.GlobalProperties.STRING_PRIVILEGE_SEPERATOR;
import static com.inlaks.access.service.constants.GlobalProperties.STRING_ROLE_SEPERATOR;
import com.inlaks.access.service.exceptions.ConfigurationPropertiesNotSetException;
import com.inlaks.access.service.interfaces.OptionsControl;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author ailori
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalConfigurationOptionsControl implements OptionsControl {

    /**
     * This tell the service in what authentication and authorization mode to
     * run in
     */
    private String serviceType;

    /**
     * The default privileges assigned to individual roles form configuration
     */
    private Map<String, List<String>> defaultConfigPrivileges;

    /**
     * configured roles
     */
    private List<String> defaultConfigRoles;

    @Override
    public <T> void buildGlobalRoleConfiguration(T roleProperty) {
        // building roles configurtation object
        if (roleProperty != null && !((String) roleProperty).isEmpty()) {
            String[] split = ((String) roleProperty).split(STRING_ROLE_SEPERATOR);
            List<String> roles = Arrays.asList(split);
            setDefaultConfigRoles(roles);
        } else {
            throw new ConfigurationPropertiesNotSetException("Roles not set for default configuration");
        }
    }

    @Override
    public <T> void buildGlobalPrivilegeConfiguration(T privilegeProperty) {
        if (privilegeProperty != null && !((String) privilegeProperty).isEmpty()) {
            String[] roles = ((String) privilegeProperty).split(STRING_ROLE_SEPERATOR);
            defaultConfigPrivileges = new HashMap<>();
            int i = 0;
            for (String role : roles) {
                String[] privileges = role.split(STRING_PRIVILEGE_SEPERATOR);
                defaultConfigPrivileges.put(defaultConfigRoles.get(i), Arrays.asList(privileges));
                i++;
            }
        } else {
            throw new ConfigurationPropertiesNotSetException("Privileges not set for default configuration");
        }
    }

}
