/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.facade;

import com.inlaks.access.service.constants.GlobalProperties;
import com.inlaks.access.service.entity.Role;
import com.inlaks.access.service.entity.User;
import com.inlaks.access.service.exceptions.ConfigurationPropertiesNotSetException;
import com.inlaks.access.service.exceptions.CreateNewUserException;
import com.inlaks.access.service.exceptions.RoleMismatchException;
import com.inlaks.access.service.exceptions.InvalidPasswordException;
import com.inlaks.access.service.mapper.AccessMapper;
import com.inlaks.access.service.model.GlobalConfigurationOptionsControl;
import com.inlaks.access.service.model.request.CreateUserRequest;
import com.inlaks.access.service.model.request.LoginRequest;
import com.inlaks.access.service.repository.DataServiceRepository;
import java.util.Arrays;
import java.util.List;
import org.inlaks.dto.BaseResponse;
import org.inlaks.hashing.payload.ValidateStringPayloadIntf;
import org.inlaks.responsecode.InlaksResponseCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ailori
 */
@Service
public class AccessFacade implements GlobalProperties {

    @Autowired
    private GlobalConfigurationOptionsControl globalConfigurationOptionsControl;

    @Autowired
    private DataServiceRepository<User> dataServiceUserRepository;

    @Autowired
    private ValidateStringPayloadIntf sha512Payload;

    @Autowired
    private AccessMapper accessMapper;

    @Autowired
    private DataServiceRepository<Role> dataServiceRoleRepository;

    public BaseResponse validateCredentials(LoginRequest request) {

        User findByEmailAndRolesId = dataServiceUserRepository.findByEmailAndRolesId(request.getEmail(), request.getRoleClaimId());

        if (findByEmailAndRolesId != null && Arrays.equals(request.getRoleClaimId(), (Long[]) accessMapper.mapRolesToLongList.apply(findByEmailAndRolesId.getRoles()).toArray())) {
            if (sha512Payload.validatePayload(findByEmailAndRolesId.getPassword(), request.getPassword())) {
                BaseResponse response = new BaseResponse();
                response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_00.getResponseCode());
                response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_00.getResponseDescription());
                return response;
            } else {
                throw new InvalidPasswordException();
            }
        } else {
            throw new RoleMismatchException();
        }

    }

    public BaseResponse createCredentials(CreateUserRequest request) {
        BaseResponse response = new BaseResponse();
        // check if requesting user has the privilege to create a new user and what type of user is this requesting user creating
        User newUser = accessMapper.mapRequestToUserRecords.apply(request);

        switch (globalConfigurationOptionsControl.getServiceType()) {
            case GLOBAL_CONFIG_DEFAULT_OPTION_TYPE:
                // get configuration roles and privileges
                List<Role> findAll = dataServiceRoleRepository.findAll();

                if (findAll.containsAll(Arrays.asList(request.getRoleClaimId()))) {
                    if (dataServiceUserRepository.save(newUser) != null) {
                        response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_00.getResponseCode());
                        response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_00.getResponseDescription());
                        return response;
                    }
                    response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_99.getResponseCode());
                    response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_99.getResponseDescription());
                    throw new CreateNewUserException(response);
                }
                throw new RoleMismatchException();
            case GLOBAL_CONFIG_CONTROLLED_OPTION_TYPE:
                // use role id to pull privileges and see if user is allowed to create a new user, this part will be done based on the requirements
                return response;
            default:
                throw new ConfigurationPropertiesNotSetException();
        }
    }
}
