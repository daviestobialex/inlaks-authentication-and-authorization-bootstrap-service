/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.repository.impl;

import com.inlaks.access.service.entity.User;
import com.inlaks.access.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ailori
 */
@Component
public class UserDataService extends Service<User> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User save(User i) {
        return userRepository.save(i);
    }
}
