/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.repository.impl;

import com.inlaks.access.service.entity.Role;
import com.inlaks.access.service.repository.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ailori
 */
@Component
public class RoleDataService extends Service<Role> {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role save(Role i) {
        return roleRepository.save(i); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Role> findAll() {
        return (List<Role>) roleRepository.findAll(); //To change body of generated methods, choose Tools | Templates.
    }

}
