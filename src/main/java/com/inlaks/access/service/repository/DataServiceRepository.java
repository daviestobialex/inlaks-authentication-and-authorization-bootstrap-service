/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.repository;

import java.util.List;

/**
 * This Generic Data Service Interface Helps Other data service classes
 *
 * @author ailori
 * @param <T>
 */
public interface DataServiceRepository<I> {

    public I save(I i);

    public List<I> findAll();

    public List<I> findAll(int pageNumber, int numberOfRecords);

    public I findByEmailAndRolesId(String email, Long[] roleIds);
    
    public void saveAll(I[] i);

}
