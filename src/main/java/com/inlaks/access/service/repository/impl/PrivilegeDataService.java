/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.repository.impl;

import com.inlaks.access.service.entity.Privilege;
import com.inlaks.access.service.repository.PrivilegeRepository;
import java.util.Iterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ailori
 */
@Component
public class PrivilegeDataService extends Service<Privilege> {

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Override
    public Privilege save(Privilege i) {
        return privilegeRepository.save(i);
    }

    @Override
    public void saveAll(Privilege[] i) {
        super.saveAll(i); //To change body of generated methods, choose Tools | Templates.
    }

}
