/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.repository;

import com.inlaks.access.service.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ailori
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    public User findByEmail(String email);

    public User findByEmailAndRolesId(String email, Long[] roleIds);
}
