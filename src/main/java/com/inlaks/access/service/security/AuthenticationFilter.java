/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.security;

import com.inlaks.access.service.constants.SecurityConstants;
import java.io.IOException;
import java.util.Arrays;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.inlaks.util.IpAddressCheckerUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

@Slf4j
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    public AuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;

        setFilterProcessesUrl(SecurityConstants.ALL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String header = request.getHeader(SecurityConstants.TOKEN_HEADER);
        String[] v = new String(Base64.getDecoder().decode(header.replace("Basic ", ""))).split(":");
        log.info("============ AUTH HEADER ===========");
        log.info(new String(Base64.getDecoder().decode(header.replace("Basic ", ""))));
        /**
         * Catch a Array Index Out of Bound Exception if wrong Authorization
         * header is passed
         */
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(v[0], v[1]);

        return authenticationManager.authenticate(authentication);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            FilterChain filterChain, Authentication authentication) throws IOException {
        IpAddressCheckerUtil ipAddressCheckerUtil = new IpAddressCheckerUtil();
//        ipAddressCheckerUtil.validateIPAddress(request.getRemoteHost().concat(";"));
        ServletRequest req = request;
        ServletResponse res = response;
        User user = ((User) authentication.getPrincipal());

        List<String> roles = user.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        log.info(Arrays.toString(roles.toArray()));
        try {
            response.setHeader("roles", Arrays.toString(roles.toArray()));
            log.info("========= handling over ==========");
            filterChain.doFilter(req, res);
        } catch (ServletException ex) {
            log.info("========= throwing error ==========");
            log.error(ex.getMessage());
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        log.info("========= handling over ?F ==========");
        super.unsuccessfulAuthentication(request, response, failed); //To change body of generated methods, choose Tools | Templates.
    }

}
