/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.security;

import com.inlaks.access.service.constants.SecurityConstants;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author t.davies
 */
@Slf4j
@Configuration
public class AuthFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            System.err.println("NO OPTIIONS");
            response.sendError(HttpServletResponse.SC_OK, "success");
            return;
        }

        String header = request.getHeader(SecurityConstants.TOKEN_HEADER);
        String[] v = new String(Base64.getDecoder().decode(header.replace("Basic ", ""))).split(":");
        log.info("============ VERIFY AUTH HEADER HERE ===========");
        log.info(new String(Base64.getDecoder().decode(header.replace("Basic ", ""))));

        filterChain.doFilter(req, res);

    }

}
