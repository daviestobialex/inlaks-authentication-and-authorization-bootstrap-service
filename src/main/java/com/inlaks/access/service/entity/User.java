package com.inlaks.access.service.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.inlaks.hashing.payload.ValidateStringPayload;
import org.inlaks.hashing.signature.ValidateWithSHA512Signature;
import org.jboss.aerogear.security.otp.api.Base32;

@Data
@Entity
@Slf4j
@Table(name = "user_account")
public class User {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    @Lob
    private String password;

    private boolean enabled;

    private boolean isUsing2FA;

    private String secret;

    //
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

    public User() {
        super();
        this.secret = Base32.random();
        this.enabled = false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User user = (User) obj;
        if (!email.equals(user.email)) {
            return false;
        }
        return true;
    }

    /**
     * Encrypts clear text password before inserting into database
     *
     * @param validateStringPayload
     */
    @PrePersist
    private void encryprPassword() {
        ValidateStringPayload validateStringPayload = new ValidateStringPayload();
        validateStringPayload.setValidateSignatureIntf(new ValidateWithSHA512Signature());
        log.info("======== ENCRYPTING PASSWORD =======");
        log.info(validateStringPayload.encryptPayload(getPassword().concat(getSecret())));
        setPassword(validateStringPayload.encryptPayload(getPassword().concat(getSecret())));
    }

}
