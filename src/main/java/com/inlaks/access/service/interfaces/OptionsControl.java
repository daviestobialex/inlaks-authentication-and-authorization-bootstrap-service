/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.interfaces;

/**
 *
 * @author ailori
 */
public interface OptionsControl {

    /**
     * builds the configuration role object
     *
     * @param roleProperty
     */
    public<T> void buildGlobalRoleConfiguration(T roleProperty);

    /**
     * build the configuration privilege object
     *
     * @param privilegeProperty
     */
    public<T> void buildGlobalPrivilegeConfiguration(T privilegeProperty);
}
