package com.inlaks.access.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InlaksAuthenticationAndAuthorizationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InlaksAuthenticationAndAuthorizationServiceApplication.class, args);
    }

}
