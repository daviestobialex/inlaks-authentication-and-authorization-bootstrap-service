/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.exceptions;

import lombok.Data;
import org.inlaks.responsecode.InlaksResponseCodeEnum;

/**
 *
 * @author ailori
 */
@Data
public class ConfigurationPropertiesNotSetException extends RuntimeException {
    private final String message;

    public ConfigurationPropertiesNotSetException() {
        super();
        this.message = InlaksResponseCodeEnum.RESPONSE_61.getResponseDescription();
    }

    public ConfigurationPropertiesNotSetException(String message) {
        super(message);
        this.message = message;
    }
}
