/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.inlaks.dto.BaseResponse;

/**
 *
 * @author ailori
 */
@Data
@AllArgsConstructor
public class CreateNewUserException extends RuntimeException {

    private final String message;

    private BaseResponse response;

    public CreateNewUserException(BaseResponse response) {
        this.message = "Could not create user account";
        this.response = response;
    }

    public CreateNewUserException() {
        this.message = null;
    }
    
    

}
