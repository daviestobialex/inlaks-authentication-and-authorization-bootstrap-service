package com.inlaks.access.service.exceptions.handler;

import com.inlaks.access.service.exceptions.CreateNewUserException;
import com.inlaks.access.service.exceptions.InvalidPasswordException;
import com.inlaks.access.service.exceptions.RoleMismatchException;
import java.lang.annotation.AnnotationFormatError;
import java.net.UnknownHostException;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.context.request.WebRequest;
import javax.validation.ConstraintViolationException;

import static java.util.stream.Collectors.joining;
import org.inlaks.dto.BaseResponse;
import org.inlaks.exceptions.UnknownIPException;
import org.inlaks.responsecode.InlaksResponseCodeEnum;

@Slf4j
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GenericExceptionHandler {

    @ExceptionHandler(UnknownIPException.class)
    public ResponseEntity<BaseResponse> handleUnknownIPException(UnknownIPException ex) {
        log.error("Unknown I.P Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AnnotationFormatError.class)
    public ResponseEntity<BaseResponse> handleUnknownIPException(AnnotationFormatError ex) {
        log.error("AnnotationFormatError Invalid Request ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<BaseResponse> handleConstraintViolationException(ConstraintViolationException ex) {
        log.error("Constraint Violation Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_27.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_27.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceAccessException.class)
    public ResponseEntity<BaseResponse> handleConnectionTimedOutException(ResourceAccessException ex) {
        log.error("Resource Access Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<BaseResponse> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
        log.error("DataIntegrity Violation Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<BaseResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.error("MethodArgumentNotValid Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<BaseResponse> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex) {
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append(ex.getContentType()).append(" media type is not supported. Supported media types are ").
                append(ex.getSupportedMediaTypes().stream().map(MediaType::getType).collect(joining(",")));
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        val format = String.format(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription(), messageBuilder.toString());

        return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UnknownHostException.class)
    public ResponseEntity<BaseResponse> handleUnknownHostException(UnknownHostException ex) {
        log.error("Unknown Host Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());
        return new ResponseEntity<>(baseResponse, HttpStatus.ACCEPTED);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<BaseResponse> handleNullPointerException(NullPointerException ex) {
        log.error("Null Pointer Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_27.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_27.getResponseDescription());
        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<BaseResponse> handleNumberFormatException(NumberFormatException ex) {
        log.error("Number Format Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_27.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_27.getResponseDescription() + ", Invalid distance value");
        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RoleMismatchException.class)
    public ResponseEntity<BaseResponse> handleRoleMismatchException(RoleMismatchException ex) {
        log.error("Role Mismatch Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_96.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_96.getResponseDescription());
        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity<BaseResponse> handleInvalidPasswordException(InvalidPasswordException ex) {
        log.error("Invalid Password Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_04.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_04.getResponseDescription());
        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CreateNewUserException.class)
    public ResponseEntity<BaseResponse> handleCreateNewUserException(CreateNewUserException ex) {
        log.error("Create New User Exception ::: {}", ex);
        return new ResponseEntity<>(ex.getResponse(), HttpStatus.BAD_REQUEST);
    }
}
