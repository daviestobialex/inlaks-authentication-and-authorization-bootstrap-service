/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.mapper;

import com.inlaks.access.service.entity.Role;
import com.inlaks.access.service.entity.User;
import com.inlaks.access.service.model.request.CreateUserRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 *
 * @author ailori
 */
@Component
public class AccessMapper {

    public Function<Collection<Role>, List<Long>> mapRolesToLongList = roles -> {
        List<Long> foundRoles = new ArrayList<>();
        roles.stream().forEach(R -> {
            foundRoles.add(R.getId());
        });
        return foundRoles;
    };

    public Function<CreateUserRequest, User> mapRequestToUserRecords = request -> {
        User record = new User();
        BeanUtils.copyProperties(request, record);
        return record;
    };

//    public Function<Map.Entry<String, List<String>>, Role> mapConfigurationToRoleRecord = config -> {
//        Role record = new Role(config.getKey());
//        return record;
//    };
}
