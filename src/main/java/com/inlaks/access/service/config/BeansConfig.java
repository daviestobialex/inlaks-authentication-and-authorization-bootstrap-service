/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.config;

import static com.inlaks.access.service.constants.GlobalProperties.DEFAULT_ADMIN_CONTROLLER_ROLE;
import static com.inlaks.access.service.constants.GlobalProperties.GLOBAL_CONFIG_CONTROLLED_OPTION_TYPE;
import static com.inlaks.access.service.constants.GlobalProperties.GLOBAL_CONFIG_DEFAULT_OPTION_TYPE;
import com.inlaks.access.service.exceptions.ConfigurationPropertiesNotSetException;
import com.inlaks.access.service.model.GlobalConfigurationOptionsControl;
import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import static com.inlaks.access.service.constants.GlobalProperties.GLOBAL_OPERATION_CONFIG_OBJECT_NAME;
import com.inlaks.access.service.repository.impl.PrivilegeDataService;
import com.inlaks.access.service.repository.impl.RoleDataService;
import com.inlaks.access.service.repository.impl.UserDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author ailori
 */
@Slf4j
@Configuration
public class BeansConfig extends RolesConfig {

    @Bean(name = GLOBAL_OPERATION_CONFIG_OBJECT_NAME)
    @Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
    public GlobalConfigurationOptionsControl setGlobalConfigurationOptions() {
        GlobalConfigurationOptionsControl goConfigurationOptionsControl = new GlobalConfigurationOptionsControl();
        if (getAuthServiceType() != null || !getAuthServiceType().isEmpty()) {

            switch (getAuthServiceType()) {
                case GLOBAL_CONFIG_DEFAULT_OPTION_TYPE:
                    goConfigurationOptionsControl.setServiceType(getAuthServiceType());
                    goConfigurationOptionsControl.buildGlobalRoleConfiguration(getAuthRoles());
                    goConfigurationOptionsControl.buildGlobalPrivilegeConfiguration(getAuthPrivileges());
                    break;
                case GLOBAL_CONFIG_CONTROLLED_OPTION_TYPE:
                    goConfigurationOptionsControl.setServiceType(getAuthServiceType());
                    goConfigurationOptionsControl.buildGlobalRoleConfiguration(DEFAULT_ADMIN_CONTROLLER_ROLE);
                    goConfigurationOptionsControl.buildGlobalPrivilegeConfiguration(DEFAULT_ADMIN_CONTROLLER_ROLE);
                    break;
                default:
                    throw new ConfigurationPropertiesNotSetException();
            }

        } else {
            throw new ConfigurationPropertiesNotSetException("Service can not start without proper configuration global properties");
        }
        return goConfigurationOptionsControl;
    }

    @Bean
    @Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
    public RoleDataService dataServiceRoleRepository() {
        return new RoleDataService();
    }

    @Bean
    @Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
    public UserDataService dataServiceUserRepository() {
        return new UserDataService();
    }
    
    @Bean
    @Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
    public PrivilegeDataService dataServicePrivilegeRepository() {
        return new PrivilegeDataService();
    }
}
