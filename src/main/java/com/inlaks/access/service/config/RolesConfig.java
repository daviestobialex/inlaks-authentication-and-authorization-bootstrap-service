package com.inlaks.access.service.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Configuration
@ConfigurationProperties
@PropertySource("file:./config/rolesConfig.properties")
public class RolesConfig {

    @NotEmpty
    private String authServiceType;

    @NotEmpty
    private String authRoles;

    @NotEmpty
    private String authPrivileges;

}
