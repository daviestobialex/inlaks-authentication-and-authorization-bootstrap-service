package com.inlaks.access.service.config;

import lombok.val;
import org.inlaks.hashing.payload.ValidateStringPayload;
import org.inlaks.hashing.payload.ValidateStringPayloadIntf;
import org.inlaks.hashing.signature.ValidateSignatureIntf;
import org.inlaks.hashing.signature.ValidateWithSHA1Signature;
import org.inlaks.hashing.signature.ValidateWithSHA512Signature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@Configuration
public class ValidateConfig {

    @Bean
    @Scope(value = SCOPE_PROTOTYPE,proxyMode = TARGET_CLASS)
    public ValidateStringPayloadIntf sha1Payload(){
        final val validateStringPayload = new ValidateStringPayload();
        validateStringPayload.setValidateSignatureIntf(validateSHA1Payload());
        return validateStringPayload;
    }

    @Bean
    @Scope(value = SCOPE_PROTOTYPE,proxyMode = TARGET_CLASS)
    public ValidateStringPayloadIntf sha512Payload(){
        final val validateStringPayload = new ValidateStringPayload();
        validateStringPayload.setValidateSignatureIntf(validateSHA512Payload());
        return validateStringPayload;
    }

    private ValidateSignatureIntf validateSHA512Payload(){
        return new ValidateWithSHA512Signature();
    }

    private ValidateSignatureIntf validateSHA1Payload(){
        return new ValidateWithSHA1Signature();
    }
}
