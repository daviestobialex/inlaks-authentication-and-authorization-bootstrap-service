/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.config;

import com.inlaks.access.service.constants.GlobalProperties;
import com.inlaks.access.service.entity.Privilege;
import com.inlaks.access.service.entity.Role;
import com.inlaks.access.service.entity.User;
import com.inlaks.access.service.exceptions.ConfigurationPropertiesNotSetException;
import com.inlaks.access.service.interfaces.OptionsControl;
import com.inlaks.access.service.model.GlobalConfigurationOptionsControl;
import com.inlaks.access.service.repository.DataServiceRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author ailori
 */
@Slf4j
@Configuration
public class PersistenceConfig implements OptionsControl, GlobalProperties {

    @Autowired
    private DataServiceRepository<Role> dataServiceRoleRepository;

    @Autowired
    private DataServiceRepository<User> dataServiceUserRepository;

    @Autowired
    private DataServiceRepository<Privilege> dataServicePrivilegeRepository;

    private List<Role> roles;

    @Autowired
    @Bean
//    @Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
    public String buildRoleAndPrivilegesEntityConfiguration(GlobalConfigurationOptionsControl globalConfigurationOptionsControl) {

        if (globalConfigurationOptionsControl != null && globalConfigurationOptionsControl.getDefaultConfigRoles() != null && globalConfigurationOptionsControl.getDefaultConfigPrivileges() != null) {
            this.buildGlobalRoleConfiguration(globalConfigurationOptionsControl.getDefaultConfigRoles());
            if (globalConfigurationOptionsControl.getServiceType().equals(GLOBAL_CONFIG_DEFAULT_OPTION_TYPE)) {
                log.info("====== STARTING SERVICE IN DEFAULT MODE =========");
                this.buildGlobalPrivilegeConfiguration(globalConfigurationOptionsControl.getDefaultConfigPrivileges());
            } else {
                log.info("====== STARTING SERVICE IN CONTROLLED MODE =========");
                log.info("====== CREATING ADMIN USER AND ROLE MODE =========");
                this.CreateAdminUser();
            }
        } else {
            throw new ConfigurationPropertiesNotSetException();
        }
        return "colors";
    }

    @Override
    public <T> void buildGlobalRoleConfiguration(T roleProperty) {
        roles = new ArrayList<>();
        ((List<String>) roleProperty).forEach((role) -> {
            roles.add(dataServiceRoleRepository.save(new Role(role)));
        });
    }

    @Override
    public <T> void buildGlobalPrivilegeConfiguration(T privilegeProperty) {
        Iterator<Role> r = roles.iterator();

        if (privilegeProperty != null) {
            while (r.hasNext()) {
                Role role = r.next();
                Collection<Privilege> privileges = new ArrayList<>();

                List<String> get = ((Map<String, List<String>>) privilegeProperty).get(role.getName());

                if (!get.isEmpty() || get != null) {
                    get.stream().forEach((V) -> {
                        privileges.add(dataServicePrivilegeRepository.save(new Privilege(V)));
                    });
                    role.setPrivileges(privileges);

                    dataServiceRoleRepository.save(role);
                }
            }
        }
    }

    /**
     * Creates a default admin user and no action can be performed without the
     * consent of this admin user
     */
    private void CreateAdminUser() {
        User user = new User();

        user.setEmail("admin@admin.com");
        user.setFirstName("admin");
        user.setLastName("admin");
        user.setRoles(roles);
        user.setUsing2FA(false);
        user.setEnabled(true);
        user.setPassword("admin");
        dataServiceUserRepository.save(user);
    }

}
