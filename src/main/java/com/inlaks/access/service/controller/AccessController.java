/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.controller;

import static com.inlaks.access.service.constants.UrlMappings.ACCESS_LOGIN;
import static com.inlaks.access.service.constants.UrlMappings.ACCESS_ROOT_MAP;
import static com.inlaks.access.service.constants.UrlMappings.ACCESS_SIGNUP;
import com.inlaks.access.service.facade.AccessFacade;
import com.inlaks.access.service.model.request.CreateUserRequest;
import com.inlaks.access.service.model.request.LoginRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ailori
 */
@RestController
@RequestMapping(ACCESS_ROOT_MAP)
public class AccessController {

    @Autowired
    private AccessFacade accessFacade;

    @ResponseBody
    @PostMapping
    @RequestMapping(ACCESS_LOGIN)
    public ResponseEntity login(@Valid @RequestBody LoginRequest request) {
        return ResponseEntity.ok().body(accessFacade.validateCredentials(request));
    }
    
    @ResponseBody
    @PostMapping
    @RequestMapping(ACCESS_SIGNUP)
    public ResponseEntity createNewUser(@Valid @RequestBody CreateUserRequest request) {
        return ResponseEntity.ok().body(accessFacade.createCredentials(request));
    }

    @ResponseBody
    @PostMapping
    @RequestMapping("/public")
    public ResponseEntity test(@Valid @RequestBody LoginRequest request) {
        return ResponseEntity.ok().body("test working");
    }
}
