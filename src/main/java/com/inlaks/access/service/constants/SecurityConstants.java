/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.constants;

/**
 * `This class is designed to load the following static properties from a
 * configuration file
 *
 * Comment out the configuration annotation once ready
 *
 * @author ailori
 */
//@Configuration
public interface SecurityConstants {

    public static final String ALL = "/*";
    // JWT token defaults
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";

}
