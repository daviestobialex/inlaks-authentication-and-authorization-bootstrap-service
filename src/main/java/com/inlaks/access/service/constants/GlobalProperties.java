/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.constants;

/**
 *
 * @author ailori
 */
public interface GlobalProperties {

    public final String GLOBAL_OPERATION_CONFIG_OBJECT_NAME = "globalProperties";

    public final String GLOBAL_CONFIG_DEFAULT_OPTION_TYPE = "default";

    public final String GLOBAL_CONFIG_CONTROLLED_OPTION_TYPE = "controlled";

    public final String STRING_ROLE_SEPERATOR = ";";

    public final String STRING_PRIVILEGE_SEPERATOR = ",";

    public final String DEFAULT_ADMIN_CONTROLLER_ROLE = "admin";

}
