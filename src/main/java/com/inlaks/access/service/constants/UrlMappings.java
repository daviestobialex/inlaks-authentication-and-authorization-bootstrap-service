/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.access.service.constants;

/**
 *
 * @author ailori
 */
public interface UrlMappings {

    public final String ACCESS_ROOT_MAP = "/v1";

    public final String ACCESS_LOGIN = "/login";
    
    public final String ACCESS_SIGNUP = "/signup";
}
