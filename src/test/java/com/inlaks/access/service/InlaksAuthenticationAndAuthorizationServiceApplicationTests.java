package com.inlaks.access.service;

import static com.inlaks.access.service.constants.GlobalProperties.STRING_ROLE_SEPERATOR;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
//@SpringBootTest
public class InlaksAuthenticationAndAuthorizationServiceApplicationTests {

    @Test
    public void contextLoads() {

        String[] split = "admin;agents".split(";");
        log.info("====== VALUES =====" + Arrays.toString(split));

    }

}
